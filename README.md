# Problema del consumidor

## Enunciado

En una mesa hay procesos que simulan el comportamiento de unos filósofos que intentan comer de un plato. Cada filósofo tiene un cubierto a su izquierda y uno a su derecha y para poder comer tiene que conseguir los dos.

Despues de comer, soltará los cubiertos y esperará para intentar coger los dos cubiertos y volver a comer.

En general todos los objetos de la clase Filósofo está en un bucle infinito dedicándose a comer y a pensar.

Simular este problema en un programa Java que muestre el progreso de todos sin caer en problemas de sincronización ni de inanición.

![img_01][img_01]

Para la creación del ejercicio debemos crear 3 clases:

* Tenedor.
* Filósofo.
* Main.

# Clase Tenedor

```java
public class Tenedor {
    private int id;
    private boolean libre = true;

    public Tenedor(int id) {
        this.id = id;
    }

    public synchronized void qTenedor(int i) throws InterruptedException {
        while (!libre)
            wait();
        System.out.println("Filosofo " + i + " coge el tenedor " + id);

        libre = false;
    }

    public synchronized void sTenedor(int i) throws InterruptedException {
        libre = true;
        System.out.println("Filosofo " + i + " suelta el tenedor " + id);
        notify();
    }
}
```

La clase __Tenedor__ tiene tres métods:
* __constructor__: inicializa la variable id que contiene el númer del tenedor.
* __qTenedor__: permite a un filósofo coger un tenedor.
* __sTenedor__: permite a un filósofo soltar un tenedor.

Los métodos _qTenedor_ y _sTenedor_ se protegen con __synchronized__.

> __Nota__: Un método en Java puede llevar el modificador  synchronized. Todos los métodos que lleven ese modificador se ejecutarán en exclusión mutua. Cuando un método sincronizado se está ejecutando, se garantiza que ningún otro método sincronizado podrá ejecutarse.

A veces nos interesa que un hilo se quede bloqueado a la espera de que ocurra algún evento, como la llegada de un dato para tratar o que el usuario termine de escribir algo en una interface de usuario. Todos los objetos java tienen el método __wait()__ que deja bloqueado al hilo que lo llama y el método __notify()__, que desbloquea a los hilos bloqueados por wait().

## Clase Filosofo

```java
import java.util.logging.Level;
import java.util.logging.Logger;

public class Filosofo extends Thread {
    private int id;
    private Tenedor izda, dcha;

    public Filosofo(int id, Tenedor izda, Tenedor dcha) {
        this.id = id;
        this.izda = izda;
        this.dcha = dcha;

    }

    public void run() {
        while(true) {
            try {
                izda.qTenedor(id);
                dcha.qTenedor(id);
                //comer
                dcha.sTenedor(id);
                izda.sTenedor(id);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
```

La clase __Filósofo__ contiene un sólo método:
* __run()__: En este método el filósofo coge el tenedor a su izquierda, luego el tenedor a su derecha, cuando tiene los dos tenedores come y, una vez ha comido, suelta los dos tenedores.

## Clase Main


```java
public class Main {

    public static void main(String[] args) {
        Tenedor[] tenedores = new Tenedor[5];

        for(int i = 0; i < tenedores.length; i++) {
            tenedores[i] = new Tenedor(i);
        }

        Filosofo[] filosofos = new Filosofo[5];

        for(int i = 0; i < tenedores.length; i++) {
            filosofos[i] = new Filosofo(i, tenedores[i], tenedores[(i+1)%5], silla);
        }

        for (int i = 0; i < filosofos.length; i++) {
            filosofos[i].start();
        }
    }

}
```

La clase __Main__ crea un array de 6 tenedores y un array de 6 filósofos, le asigna a cada uno de los filósofos su tenedor izquierdo y su tenedor derecho y llama al método _run()_ de cada uno de los filósofos mediante el método _start()_.

## Ejecución

Si ejecutamos la aplicación obtenemos la siguiente salida:

```bash
run:
Filosofo 1 coge el tenedor 1
Filosofo 3 coge el tenedor 3
Filosofo 2 coge el tenedor 2
Filosofo 0 coge el tenedor 0
Filosofo 4 coge el tenedor 4
```

El problema que se presenta es que cada filósofo ha cogido un tenedor y no lo suelta y está esperando por el segundo tenedor, con lo cual todos mueren por inanición.

Para solucionar esto, se añade un elemento extra a la práctica que es el elemento __silla__: para que un filósofo pueda comer, necesita primero sentarse en una silla. Si hay 5 filósofos, habrá 4 sillas, de manera que uno se quedará esperando de pie a que otro filósofo acabe de comer.


## Clase Silla

```java
public class Silla {
    private int sillasLibres = 4;

    public synchronized void cogeSilla(int i) throws InterruptedException {
        while (sillasLibres == 0)
            wait();
        System.out.println("Filosofo " + i + " coge una silla");
        sillasLibres--;
    }

    public synchronized void sueltaSilla(int i) {
        sillasLibres++;
        System.out.println("Filosofo " + i + " suelta una silla");
        notify();
    }
}
```

La clase __Silla__ cuenta con 4 sillas y tiene dos métodos:

* __cogerSilla__: permite a un filósofo coger una silla libre.
* __sueltaSilla__: permite a un filófoso soltar una silla.

Ambos métodos están protegidos con _synchronized_, es decir, sólo un filósofo puede coger una silla o soltar una silla a la vez.

> __Nota__: al estar protegidos los métodos _cogerSilla_ y _sueltaSilla_ con _synchronized_, . los objectos instanciados de esta clase no podrán ejecutar estos métodos en paralelo.

## Nueva Clase Filósofo

```java
import java.util.logging.Level;
import java.util.logging.Logger;

public class Filosofo extends Thread {
    private int id;
    private Tenedor izda, dcha;
    private Silla silla;

    public Filosofo(int id, Tenedor izda, Tenedor dcha, Silla silla) {
        this.id = id;
        this.izda = izda;
        this.dcha = dcha;
        this.silla = silla;
    }

    public void run() {
        while(true) {
            try {
                silla.cogeSilla(id);
                izda.qTenedor(id);
                dcha.qTenedor(id);
                //comer
                dcha.sTenedor(id);
                izda.sTenedor(id);
                silla.sueltaSilla(id);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
```

En la nueva clase __Filosofo__, el filósofo, antes de coger los tenedores, tiene que sentarse en una silla.

## Nueva Clase Main


```java
public class Main {

    public static void main(String[] args) {
        Silla silla = new Silla();
        Tenedor[] tenedores = new Tenedor[5];

        for(int i = 0; i < tenedores.length; i++) {
            tenedores[i] = new Tenedor(i);
        }

        Filosofo[] filosofos = new Filosofo[5];

        for(int i = 0; i < tenedores.length; i++) {
            filosofos[i] = new Filosofo(i, tenedores[i], tenedores[(i+1)%5], silla);
        }

        for (int i = 0; i < filosofos.length; i++) {
            filosofos[i].start();
        }
    }

En la nueva clase __Main__, se crea un objeto de tipo _Silla_:

}
```


## Ejecución

Si ejecutamos la aplicación obtenemos la siguiente salida:

```bash
run:
0: Productor produce 25
1: Productor produce 58
2: Productor produce 25
3: Productor produce 69
0: Consumidor consume 25
1: Consumidor consume 58
4: Productor produce 85
5: Productor produce 76
6: Productor produce 12
2: Consumidor consume 25
7: Productor produce 0
8: Productor produce 91
9: Productor produce 9
3: Consumidor consume 69
4: Consumidor consume 85
10: Productor produce 75
11: Productor produce 60
5: Consumidor consume 76
6: Consumidor consume 12
7: Consumidor consume 0
8: Consumidor consume 91
12: Productor produce 69
13: Productor produce 46
14: Productor produce 14
15: Productor produce 60
9: Consumidor consume 9
10: Consumidor consume 75
16: Productor produce 93
11: Consumidor consume 60
17: Productor produce 67
12: Consumidor consume 69
18: Productor produce 13
13: Consumidor consume 46
19: Productor produce 25
14: Consumidor consume 14
15: Consumidor consume 60
20: Productor produce 43
21: Productor produce 8
22: Productor produce 34
16: Consumidor consume 93
17: Consumidor consume 67
23: Productor produce 87
24: Productor produce 90
18: Consumidor consume 13
19: Consumidor consume 25
25: Productor produce 60
20: Consumidor consume 43
26: Productor produce 83
21: Consumidor consume 8
27: Productor produce 56
22: Consumidor consume 34
28: Productor produce 24
23: Consumidor consume 87
29: Productor produce 64
24: Consumidor consume 90
30: Productor produce 89
25: Consumidor consume 60
31: Productor produce 35
26: Consumidor consume 83
32: Productor produce 49
27: Consumidor consume 56
33: Productor produce 29
28: Consumidor consume 24
34: Productor produce 12
29: Consumidor consume 64
30: Consumidor consume 89
35: Productor produce 2
31: Consumidor consume 35
36: Productor produce 39
32: Consumidor consume 49
37: Productor produce 78
33: Consumidor consume 29
34: Consumidor consume 12
38: Productor produce 35
35: Consumidor consume 2
36: Consumidor consume 39
37: Consumidor consume 78
38: Consumidor consume 35
39: Productor produce 97
40: Productor produce 57
41: Productor produce 88
39: Consumidor consume 97
42: Productor produce 32
43: Productor produce 4
44: Productor produce 89
45: Productor produce 27
40: Consumidor consume 57
41: Consumidor consume 88
46: Productor produce 5
42: Consumidor consume 32
43: Consumidor consume 4
44: Consumidor consume 89
45: Consumidor consume 27
47: Productor produce 71
46: Consumidor consume 5
48: Productor produce 41
47: Consumidor consume 71
48: Consumidor consume 41
49: Productor produce 2
50: Productor produce 82
51: Productor produce 64
49: Consumidor consume 2
52: Productor produce 34
50: Consumidor consume 82
53: Productor produce 16
51: Consumidor consume 64
54: Productor produce 63
52: Consumidor consume 34
55: Productor produce 5
53: Consumidor consume 16
56: Productor produce 64
57: Productor produce 11
58: Productor produce 75
59: Productor produce 61
60: Productor produce 3
54: Consumidor consume 63
55: Consumidor consume 5
56: Consumidor consume 64
57: Consumidor consume 11
58: Consumidor consume 75
59: Consumidor consume 61
61: Productor produce 54
62: Productor produce 59
63: Productor produce 87
64: Productor produce 82
65: Productor produce 78
60: Consumidor consume 3
61: Consumidor consume 54
62: Consumidor consume 59
63: Consumidor consume 87
64: Consumidor consume 82
66: Productor produce 93
67: Productor produce 18
68: Productor produce 5
69: Productor produce 33
70: Productor produce 76
65: Consumidor consume 78
66: Consumidor consume 93
67: Consumidor consume 18
68: Consumidor consume 5
71: Productor produce 63
69: Consumidor consume 33
72: Productor produce 88
70: Consumidor consume 76
73: Productor produce 96
71: Consumidor consume 63
74: Productor produce 4
72: Consumidor consume 88
75: Productor produce 83
73: Consumidor consume 96
74: Consumidor consume 4
75: Consumidor consume 83
76: Productor produce 11
77: Productor produce 19
76: Consumidor consume 11
78: Productor produce 91
79: Productor produce 88
80: Productor produce 4
77: Consumidor consume 19
78: Consumidor consume 91
81: Productor produce 3
82: Productor produce 43
83: Productor produce 65
84: Productor produce 72
79: Consumidor consume 88
80: Consumidor consume 4
85: Productor produce 67
86: Productor produce 8
87: Productor produce 83
81: Consumidor consume 3
82: Consumidor consume 43
83: Consumidor consume 65
84: Consumidor consume 72
85: Consumidor consume 67
86: Consumidor consume 8
88: Productor produce 50
89: Productor produce 8
90: Productor produce 3
91: Productor produce 80
92: Productor produce 57
87: Consumidor consume 83
88: Consumidor consume 50
89: Consumidor consume 8
90: Consumidor consume 3
91: Consumidor consume 80
93: Productor produce 96
94: Productor produce 74
95: Productor produce 19
96: Productor produce 63
97: Productor produce 10
92: Consumidor consume 57
93: Consumidor consume 96
94: Consumidor consume 74
95: Consumidor consume 19
96: Consumidor consume 63
98: Productor produce 42
99: Productor produce 56
97: Consumidor consume 10
98: Consumidor consume 42
99: Consumidor consume 56
```


[img_01]: img/01.png "Problema de los Filósofos"
